# -*- coding: utf-8 -*-
'''Take the number 192 and multiply it by each of 1, 2, and 3:

192 × 1 = 192
192 × 2 = 384
192 × 3 = 576
By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?'''
# coding: utf-8
from time import clock
timeStart = clock()
storeSet  = set()
for x in xrange(10000):
    for n in xrange(2, 9 / len(str(x)) + 1):
        num = str()
        for i in xrange(n):
            num += str(x * (i + 1))
        if len(num) == 9 and len(set(num)) == 9 and not '0' in num:
            storeSet.add(int(num))
print max(storeSet)
print 'Run time: %.5fs.' % (clock() - timeStart)
