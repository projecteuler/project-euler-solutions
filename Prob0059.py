# -*- coding: utf-8 -*-
'''Each character on a computer is assigned a unique code and the preferred standard is ASCII (American Standard Code for Information Interchange). For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.

A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value, taken from a secret key. The advantage with the XOR function is that using the same encryption key on the cipher text, restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.

For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random bytes. The user would keep the encrypted message and the encryption key in different locations, and without both "halves", it is impossible to decrypt the message.

Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key. If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message. The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.

Your task has been made easy, as the encryption key consists of three lower case characters. Using cipher1.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, and the knowledge that the plain text must contain common English words, decrypt the message and find the sum of the ASCII values in the original text.'''
# TARGET: 1. Get the cipher
#         2. Find the 3-digit key to the cipher
#         3. Decrypt it
#         4. Get the sum of the ASCII values in the message text
# METHOD: 1* Get the cipher with 'urllib.urlopen'
#         2* Loop all the possible 3-digit string:
#             2-1* For the first 'testLength' chars in the cipher text,
#                   XOR decrypt it with current 3-digit string
#             2-2* If every chars in the almost message are letters or
#                   punctuations, store the key and then break the loop
#             2-3* Otherwise, continue
#         3* Loop the cipher text, use the key to decrypt it
#         4* Get the sum of the ASCII values in the message text
from time import clock
from urllib import urlopen
import string
cipherURL      = 'http://projecteuler.net/project/cipher1.txt'
rawData        = urlopen(cipherURL).read()
rawIntList     = list(int(strNum) for strNum in rawData[:-1].split(','))
lowerCaseASCII = list(ord(char) for char in string.lowercase)
testLength     = len(rawIntList) // 10
keyASCIIList   = list()
timeStart      = clock()
# Loop Invariant: the index of the char in the key-string.
#   start: 0, the begining
#   pross: apparently true
#   stop : 2, the index of the last char in the key-string
for indexOfKey in xrange(3):
    for keyASCII in lowerCaseASCII:
        for charASCII in rawIntList[indexOfKey:testLength:3]:
            XORresult = charASCII ^ keyASCII
            if not chr(XORresult) in string.letters + string.digits + " ,.:;?!'()":
                # test next lowercase char
                break
        else:
            # store current char at the very position of the key
            keyASCIIList.append(keyASCII)
            # test next position of key
            break
print 'The key is:', ''.join(list(chr(keyASCII) for keyASCII in keyASCIIList))
print 'The answer is:', sum(list(rawIntList[index] ^ keyASCIIList[index % 3]\
                            for index in xrange(len(rawIntList))))
print 'Run time: %.5fs.' % (clock() - timeStart)
