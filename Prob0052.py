# -*- coding: utf-8 -*-
'''It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.'''
# coding: utf-8
from time import clock
storeSet  = set()
base      = 10 ** 5
timeStart = clock()
for n in xrange(base, 2 * base):
    if set(str(2 * n)) == set(str(n)) and set(str(3 * n)) == set(str(n)) and set(str(4 * n)) == set(str(n)) and set(str(5 * n)) == set(str(n)) and set(str(6 * n)) == set(str(n)):
        print n
        break
print 'Run time: %.5fs.' % (clock() - timeStart)
