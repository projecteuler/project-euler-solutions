from math import factorial

combinatoric_selection = \
    lambda n, k: factorial(n) / (factorial(k) * factorial(n - k))

if __name__ == '__main__':
    print factorial(10)
