# -*- coding: utf-8 -*-
from math import ceil, sqrt

def prime_factors_num(supNum = 10 ** 6):
    workingList = get_prime_test_list(supNum)
    countList   = [0] * supNum
    for wking in xrange(2, supNum):
        if workingList[wking]: # if wking is prime
            countList[wking::wking] = \
              map(lambda num: num + 1, countList[wking::wking])
    return countList

def get_prime_test_list(supNum = 10 ** 6):
    index       = [1] * (supNum + 1)
    index[4::2] = [0] * (supNum // 2 - 1)
    for i in xrange(3, int(ceil(sqrt(supNum))), 2):
        if index[i]:
            index[i ** 2::i] = [0] * (supNum // i - i + 1)
    index[0], index[1] = 0, 0
    return index

def sieve_prime(supNum = 10 ** 6):
    ptList = get_prime_test_list(supNum)
    return [x for x in xrange(supNum) if ptList[x]]

def prime_test(intNum):
    if intNum == 1:
        return False
    elif intNum == 2:
        return True
    elif not intNum % 2:
        return False
    for i in xrange(3, int(intNum ** 0.5) + 1, 2):
        if not intNum % i:
            return False
    return True

def get_prime_factors(num):
    if num == 1:
        return None
    working = 2
    primeFactorsDict = dict()
    while working ** 2 <= num:
        count = 0
        while not num % working:
            count += 1
            num //= working
        else:
            if count:
                primeFactorsDict[working] = count
            working += 1
    else:
        if num > 1:
            primeFactorsDict[num] = 1
        return primeFactorsDict

def num_of_factors(num):
    if num == 1:
        return 1
    working = 2
    nof = 1
    while working ** 2 <= num:
        current = 0
        while not num % working:
            current += 1
            num //= working
        nof *= (current + 1)
        working += 1
    if num > 1:
        nof *= 2
    return nof

def get_proper_factors_div(intNum):
    if intNum == 1:
        return None
    root     = int(intNum ** 0.5) + 1
    storeSet = set()
    storeSet.add(1)
    for i in xrange(2, root):
        if not intNum % i:
            storeSet.add(i)
            storeSet.add(intNum / i)
    return sorted(storeSet)

def get_proper_factors(intNum):
    if intNum == 1:
        return None
    storeList = list()
    handle_prime_factors(get_prime_factors(intNum), 1, storeList)
    return sorted(storeList[:-1])

def handle_prime_factors(workingDict, res, storeList):
    if not len(workingDict):
        storeList.append(res)
        return storeList
    currentKey = workingDict.keys()[0]
    for i in range(workingDict[currentKey] + 1):
        temp = currentKey ** i
        handle_prime_factors(dict([(key, workingDict[key])for key in workingDict.keys() if not key == currentKey]), res * temp, storeList)

def get_lcm(*numList):
    storeDict = dict()
    for x in numList:
        workingDict = get_prime_factors(x)
        if not workingDict:
            pass
        else:
            for prime, power in workingDict.items():
                if storeDict.has_key(prime):
                    storeDict[prime] = max(storeDict[prime], workingDict[prime])
                else:
                    storeDict.update({prime : power})
    prod = 1
    for prime, power in storeDict.items():
        prod *= prime ** power
    return prod

def get_gcd(*numList):
    working = 1
    for x in numList:
        working *= x
    return working / get_lcm(*numList)

if __name__ == '__main__':
    from time import clock
    testNum = 10 ** 6 - 1
    start   = clock()
    get_prime_test_list(testNum)
    stopOne = clock()
    prime_test(testNum)
    stopTwo = clock()
    print stopOne - start
    print stopTwo - stopOne
