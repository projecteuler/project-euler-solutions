# -*- coding: utf-8 -*-
'''The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?'''
from time import clock
storeSet  = set()
timeStart = clock()
index     = 1
while 1:
    currentPenta = (3 * index ** 2 - index) / 2
    storeSet.add(currentPenta)
    for iterPenta in storeSet:
        if (currentPenta - iterPenta) in storeSet and (currentPenta - 2 * iterPenta) in storeSet:
            print currentPenta - 2 * iterPenta
            break
    else:
        index += 1
        continue
    break
print 'Run time: %.5fs.' % (clock() - timeStart)
