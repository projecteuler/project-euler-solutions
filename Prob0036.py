# -*- coding: utf-8 -*-
'''The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)'''
SUP    = 1 + 10 ** 6
dbpSum = 0

for x in xrange(SUP):
    if str(x) == str(x)[::-1] and bin(x)[2:] == bin(x)[2:][::-1]:
        dbpSum += x

if __name__ == '__main__':
    print dbpSum
