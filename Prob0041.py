# -*- coding: utf-8 -*-
'''We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?'''
# coding: utf-8
from time import clock
from Primes import prime_test as pt
SUP       = 7654321
timeStart = clock()
for x in xrange(SUP, 2143, -2):
    if pt(x) and set(str(x)) == set([str(i) for i in xrange(1, len(str(x)) + 1)]):
        print x
        break
print 'Run time: %.5fs.' % (clock() - timeStart)
