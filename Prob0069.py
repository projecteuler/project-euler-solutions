# -*- coding: utf-8 -*-
'''Euler's Totient function, φ(n) [sometimes called the phi function], is used to determine the number of numbers less than n which are relatively prime to n. For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, φ(9)=6.

n   Relatively Prime    φ(n)    n/φ(n)
2   1   1   2
3   1,2 2   1.5
4   1,3 2   2
5   1,2,3,4 4   1.25
6   1,5 2   3
7   1,2,3,4,5,6 6   1.1666...
8   1,3,5,7 4   2
9   1,2,4,5,7,8 6   1.5
10  1,3,7,9 4   2.5
It can be seen that n=6 produces a maximum n/φ(n) for n ≤ 10.

Find the value of n ≤ 1,000,000 for which n/φ(n) is a maximum.'''
from time import clock
from Primes import prime_test
# if n = \prod_{k = 1}^r p_k^{\alpha_k}, then we have
#     \phi(n) = \prod_{k = 1}^r \Big((p_k - 1) p_k^{\alpha_k - 1}\Big)
# i.e.
#     \phi(n) = n \cdot \prod_{p | n} \frac{p - 1}{p}
# hence
#     \frac{n}{\phi(n)} = \prod_{p | n} \frac{p}{p - 1}

# the value \phi(n) is determined by all n's prime factors
# Thus, if we denote p_k as the k-th prime number and S_k as the sum of
#   first k prime numbers, S_l is what we need if S_l <= SUP < S_{l + 1}
SUP       = 1 + 10 ** 6
working   = 2
prod      = 1
timeStart = clock()
while 1:
    if prime_test(working):
        prod *= working
        if prod > SUP:
            prod /= working
            break
    working += 1
print prod
print 'Run time: %.3fms.' % ((clock() - timeStart) * 1000)
