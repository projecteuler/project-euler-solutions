# -*- coding: utf-8 -*-
'''The 5-digit number, 16807=75, is also a fifth power. Similarly, the 9-digit number, 134217728=89, is a ninth power.

How many n-digit positive integers exist which are also an nth power?'''
from time import clock
counter   = 0
timeStart = clock()
for num in xrange(1, 10):
    power = 1
    while power == len(str(num ** power)):
        # If power > len(str(num ** power)), we have for every power' > power,
        # power' > len(str(num ** power')).
        # Thus, the loop should be stopped here.
        counter += 1
        power   += 1
print counter
print 'Run time: %.5fs.' % (clock() - timeStart)
