# -*- coding: utf-8 -*-
'''
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
'''

from Primes import get_prime_factors as gpf

if __name__ == '__main__':
    targetNum = 600851475143
    print max(gpf(targetNum).keys())
