# -*- coding: utf-8 -*-
'''If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?'''
# coding: utf-8
from time import clock
import math

storeDict = dict()
SUP       = 1000
startTime = clock()
for shortSum in range(2, SUP / 2):
    for a in range(1, int(shortSum / 2)):
        z = math.sqrt((a ** 2 + (shortSum - a) ** 2))
        if math.ceil(z) == z:
            if storeDict.has_key(shortSum + int(z)):
                storeDict[shortSum + int(z)] += 1
            else:
                storeDict[shortSum + int(z)] = 1

solution = max(storeDict, key = storeDict.get)

print 'Run time: %.5f' % (clock() - startTime)
print 'for perimeter = %s, max number of solution: %s' % (solution, storeDict[solution])
