# -*- coding: utf-8 -*-
'''The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.

Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?'''
from urllib import urlopen
rawSource = urlopen('http://projecteuler.net/project/words.txt').read()
dataList  = rawSource[1:-1].split('","')
baseNum   = ord('A') - 1
triSet    = (lambda n: set([int(0.5 * x * (x + 1)) for x in xrange(n)]))(100)
count     = 0
for word in dataList:
    wordSum = 0
    for char in word:
        wordSum += ord(char) - baseNum
    if wordSum in triSet:
        count += 1
if __name__ == '__main__':
    print count
