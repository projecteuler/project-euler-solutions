# -*- coding: utf-8 -*-
'''A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.

The text file, keylog.txt, contains fifty successful login attempts.

Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.'''
from time import clock
from urllib import urlopen
from itertools import permutations

def check_order(code, rawSet):
    '''
    TARGET: Check if the certain 'code' match the order displayed in
            the numset 'rawSet'.
    METHOD: 1* Get one number from rawSet,
            2* Get one digit from the very number,
            3* Check if this digit is in the code,
                3-1, if not, return False
                3-2, if yes, slice 'code', loop from step 2
            4* loop from step 1
            5* return True
    '''
    for n in rawSet:
        tmp = code
        for i in n:
            if i in tmp:
                tmp = tmp[tmp.find(i) + 1:]
            else:
                return False
    return True
rawData   = urlopen('http://projecteuler.net/project/keylog.txt').read()
rawSet    = set(rawData[:-1].split('\n'))
timeStart = clock()
digiSet   = set("".join(rawSet))
# Here is an assumption: every digit should, and should only, appears once in
# the passcode. However, I could not prove it, though the answer is correct.
for p in permutations(digiSet):
    # permutations returns a iterator, contains the permutations in the digiSet
    code  = ''.join(p)
    if check_order(code, rawSet):
        break
print code
print 'Run time: %.5fs.' % (clock() - timeStart)
