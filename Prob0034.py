# -*- coding: utf-8 -*-
'''145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.'''
def sum_factorial(L):
    sumFactorial = 0
    for i in L:
        fact = 1
        for j in range(1, (int(i) + 1)):
            fact = fact * j
        sumFactorial = sumFactorial + fact
    return sumFactorial

def main():
    dfSum = 0
    for n in range(1, 5 * 10 ** 4):
        L = str(n)
        x = sum_factorial(str(n))
        if x == n:
            dfSum += x
    print dfSum - 3
    # subract 3 for 1! and 2!

if __name__ == '__main__':
    main()
