# -*- coding: utf-8 -*-
'''We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.

HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.'''
# coding: utf-8
from math import sqrt, ceil
from time import clock
storeSet = set()
timeStart = clock()
for n in xrange(1000, 8000):
    if not '0' in str(n) and len(str(n)) == len(set(str(n))):
        for i in xrange(2, int(ceil(sqrt(n)))):
            if not n % i:
                nums = str(i) + str(n / i) + str(n)
                if not '0' in str(nums) and len(nums) == 9 and len(str(nums)) == len(set(str(nums))):
                    storeSet.add(n)
print sum(storeSet)
print 'Run time: %.5fs.' % (clock() - timeStart)
