# -*- coding: utf-8 -*-
'''It is possible to show that the square root of two can be expressed as an infinite continued fraction.

√ 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...

By expanding this for the first four iterations, we get:

1 + 1/2 = 3/2 = 1.5
1 + 1/(2 + 1/2) = 7/5 = 1.4
1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...

The next three expansions are 99/70, 239/169, and 577/408, but the eighth expansion, 1393/985, is the first example where the number of digits in the numerator exceeds the number of digits in the denominator.

In the first one-thousand expansions, how many fractions contain a numerator with more digits than denominator?'''
# TARGET: 1. Compute the first one thousand expansions
#         2. Compare the numerator and the denominator
#         3. Count the number of the expansions whose number of
#             digits in the numerator exceeds the number
#             of digits in the denominator.
# NOTE  : 1. Denote a_n as the numerators, b_n as the denominator of the n-th
#             expansion
#         2. a_1 = 3, a_2 = 7, a_3 = 17, a_4 = 41, ...
#         3. b_1 = 2, b_2 = 5, b_3 = 12, b_4 = 29, ...
#         4. a_(n + 1) = a_n + 2 * b_n; b_(n + 1) = a_n + b_n
from time import clock
SUP                    = 1000
counter                = 0
numerator, denominator = 1, 1 # the 0-th expansion is 1 / 1.
timeStart              = clock()
# Loop Invariant: the number of the expansions that we have computed
# Start: 0, yep
# Pross: Apparently, yep
# Stop : SUP, yep
for working in xrange(SUP):
    # the (working - 1)-th expansion
    numerator, denominator = numerator + 2 * denominator, \
        numerator + denominator
    # the (working)-th expansion
    #
    # BOOL and INT A or INT B
    # if True: return A
    # else: return B
    counter += (len(str(numerator)) > len(str(denominator))) and 1 or 0
print counter
print 'Run time: %.5fs.' % (clock() - timeStart)
