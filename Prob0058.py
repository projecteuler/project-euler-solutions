# -*- coding: utf-8 -*-
'''Starting with 1 and spiralling anticlockwise in the following way, a square spiral with side length 7 is formed.

37 36 35 34 33 32 31
38 17 16 15 14 13 30
39 18  5  4  3 12 29
40 19  6  1  2 11 28
41 20  7  8  9 10 27
42 21 22 23 24 25 26
43 44 45 46 47 48 49

It is interesting to note that the odd squares lie along the bottom right diagonal, but what is more interesting is that 8 out of the 13 numbers lying along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.

If one complete new layer is wrapped around the spiral above, a square spiral with side length 9 will be formed. If this process is continued, what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below 10%?'''
from time import clock
from Primes import prime_test as pt
currentOdd    = 1
currentLength = 3
counterPrimes = 0
timeStart     = clock()
# Loop Invariant: counterPrimes: the number of primes at the corners
#                 currentLength: current side length
# Start: counterPrimes = 0, currentLength = 1, yep
# Pross: currentLength += 2, apparently yep
# Stop : currentLength: the length of square spiral
#         for which the ratio of primes along both diagonals
#         first falls below 10% .
while 1:
    # Loop Invariant: corner: the number of odd numbers at corners
    #                           that we have computed
    # Start: corner = 0, yep
    # Pross: yep
    # Stop : corner = 4, yep
    for corner in xrange(4):
        currentOdd += currentLength - 1
        # BOOL and INT A or INT B
        # if true: return A
        # else: return B
        counterPrimes += pt(currentOdd) and 1 or 0
    if counterPrimes / float(2 * currentLength - 1) < 0.1:
        break
    else:
        currentLength += 2
print currentLength
print 'Run time: %.5fs.' % (clock() - timeStart)
