# -*- coding: utf-8 -*-
'''In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by only moving to the right and down, is indicated in bold red and is equal to 2427.


131 673 234 103 18
201 96  342 965 150
630 803 746 422 111
537 699 497 121 956
805 732 524 37  331

Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target As...'), a 31K text file containing a 80 by 80 matrix, from the top left to the bottom right by only moving right and down.'''
from time import clock
from urllib import urlopen
matrixURL = 'http://projecteuler.net/project/matrix.txt'
rawMatrix = urlopen(matrixURL).read()[:-1]
matrix    = [[int(element) for element in line.split(',')]\
    for line in rawMatrix.split('\n')]
axisX     = len(matrix)
axisY     = len(matrix[0])
timeStart = clock()
for x in xrange(axisX):
    for y in xrange(axisY):
        if x == y == 0: # both unavalible
            continue
        elif x == 0 and y != 0: # up is not availible
            matrix[x][y] += matrix[x][y - 1]
        elif x != 0 and y == 0: # left is not availible
            matrix[x][y] += matrix[x - 1][y]
        else: # both availible
            matrix[x][y] += min(matrix[x][y - 1], matrix[x - 1][y])
print matrix[axisX - 1][axisY - 1]
print 'Run time: %.3fms.' % ((clock() - timeStart) * 1000)
