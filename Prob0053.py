# -*- coding: utf-8 -*-
'''A googol (10100) is a massive number: one followed by one-hundred zeros; 100100 is almost unimaginably large: one followed by two-hundred zeros. Despite their size, the sum of the digits in each number is only 1.

Considering natural numbers of the form, ab, where a, b < 100, what is the maximum digital sum?'''
# coding: utf-8
from time import clock
from Combination import combinatoric_selection as cs
count     = 0
base      = 10 ** 6
timeStart = clock()
for n in xrange(10, 101):
    currentCount = 0
    for k in xrange(n, 0, -1):
        count += cs(n, k) >= base and 1 or 0
print 'Run time: %.5fs.' % (clock() - timeStart)
print count
