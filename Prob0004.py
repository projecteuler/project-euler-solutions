# -*- coding: utf-8 -*-
'''
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
'''
if __name__ == '__main__':
    palNum = list()
    for x in xrange(100, 1000):
        for y in xrange(100, 1000):
            prod = str(x * y)
            if prod == prod[::-1]:
                palNum.append(int(prod))
    print max(palNum)
