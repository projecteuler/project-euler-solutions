# -*- coding: utf-8 -*-
'''
The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the first term in the Fibonacci sequence to contain 1000 digits?
'''

from Fibonacci import fibonacci_iterative as fiter

def fibonacci_digi_sup_count(SUP):
    first, second = 1, 1
    count         = 2
    while len(str(second)) < SUP:
        first, second = fiter(first, second)
        count        += 1
    return count

if __name__ == '__main__':
    print fibonacci_digi_sup_count(1000)
