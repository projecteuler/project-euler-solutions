# -*- coding: utf-8 -*-
'''It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 2×12
15 = 7 + 2×22
21 = 3 + 2×32
25 = 7 + 2×32
27 = 19 + 2×22
33 = 31 + 2×12

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?'''
from time import clock
from Primes import get_prime_test_list

def check(n, ptList):
    for p in xrange(3, n, 2):
        if ptList[p]:
            r = (n - p) / 2
            if int(r ** 0.5) ** 2 == r:
                return False
    return True
currentOdd = 9
timeStart  = clock()
ptList     = get_prime_test_list(6 * 10 ** 3)
while 1:
    if not ptList[currentOdd] and check(currentOdd, ptList):
        break
    currentOdd += 2
print 'Run time: %.5fs.' % (clock() - timeStart)
print currentOdd
