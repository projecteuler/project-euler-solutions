# -*- coding: utf-8 -*-
'''A googol (10100) is a massive number: one followed by one-hundred zeros; 100100 is almost unimaginably large: one followed by two-hundred zeros. Despite their size, the sum of the digits in each number is only 1.

Considering natural numbers of the form, ab, where a, b < 100, what is the maximum digital sum?'''
# coding: utf-8
from time import clock
currentMax = 0
timeStart  = clock()
for a in xrange(100):
    for b in xrange(100):
        digSum = 0
        for dig in str(a ** b):
            digSum += int(dig)
        currentMax = digSum > currentMax and digSum or currentMax
print currentMax
print 'Run time: %.5fs.' % (clock() - timeStart)
