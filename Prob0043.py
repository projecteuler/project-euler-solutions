# -*- coding: utf-8 -*-
'''The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.

Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:

d2d3d4=406 is divisible by 2
d3d4d5=063 is divisible by 3
d4d5d6=635 is divisible by 5
d5d6d7=357 is divisible by 7
d6d7d8=572 is divisible by 11
d7d8d9=728 is divisible by 13
d8d9d10=289 is divisible by 17
Find the sum of all 0 to 9 pandigital numbers with this property.'''
from time import clock
resPrimeList = [13, 11, 7, 5, 3, 2, 1]
index        = [i < 100 and '0' + str(i) or str(i) for i in xrange(17, 1000, 17)]
timeStart    = clock()
for num in resPrimeList:
    tempList = list()
    for presence in (i < 100 and '0' + str(i) or str(i) for i in xrange(num, 1000, num)):
        for segment in index:
            if presence[-2:] == segment[:2] and len(presence[0] + segment) == len(set(presence[0] + segment)):
                tempList.append(presence[0] + segment)
    index = tempList
ssdSum = 0
for n in index:
    if n[0] != "0":
        ssdSum += int(n)
print 'Run time: %.5fs.' % (clock() - timeStart)
print ssdSum
