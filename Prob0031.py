# -*- coding: utf-8 -*-
'''In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?'''
def coin_sum_count(coinList, count, res):
    if len(coinList) == 1 and not res % coinList[0]:
        count += 1
    elif len(coinList) > 1:
        coinList = sorted(coinList)[::-1]
        for x in xrange(int((res + coinList[0] - 1) / coinList[0])):
            count = coin_sum_count(coinList[1:], count, res - x * coinList[0])
        if not res % coinList[0]:
            count += 1
    return count
if __name__ == '__main__':
    print coin_sum_count([200, 5, 100, 50, 20, 10, 2, 1], 0, 200)
