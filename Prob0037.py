# -*- coding: utf-8 -*-
'''The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.'''
from time import clock
from Primes import get_prime_test_list as gptl
def is_both_truncatable(intNum, ptList):
    strNum = str(intNum)
    for x in xrange(1, len(strNum)):
        if not (ptList[int(strNum[x:])] and ptList[int(strNum[:-x])]):
            return False
    return True
SUP       = int(7.4 * 10 ** 5)
btpSet    = set()
startTime = clock()
ptList    = gptl(SUP)
for x in xrange(23, SUP, 2):
    if ptList[x] and not '0' in str(x) and not '2' in str(x)[1:] and not '4' in str(x) and not '5' in str(x)[1:] and not '6' in str(x) and not '8' in str(x) and is_both_truncatable(x, ptList):
        btpSet.add(x)
print 'It takes %.5f second(s).' % (clock() - startTime)
print 'The answer is %d.' % (sum(btpSet))
