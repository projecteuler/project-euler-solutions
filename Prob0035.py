# -*- coding: utf-8 -*-
'''The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?'''
from Primes import get_prime_test_list as gptl

SUP    = 10 ** 6
def is_circular_prime(primeTestList, number):
    """Check if number is a circular prime.

    Keyword arguments:
    primeTestList -- a list from 0..n with boolean values that indicate if
              i in 0..n is a prime
    number        -- the integer you want to check
    """
    number = str(number)
    for i in xrange(0, len(number)):
        rotatedNumber = number[i:] + number[:i]
        if not primeTestList[int(rotatedNumber)]:
            return False
    return True

if __name__ == "__main__":
    primeTestList  = gptl(SUP)
    numberOfPrimes = 2 # for 2, 5
    for prime, isPrime in enumerate(primeTestList):
        if (not isPrime) or ("2" in str(prime)) or \
           ("4" in str(prime)) or ("6" in str(prime)) or \
           ("8" in str(prime)) or ("0" in str(prime)) or \
           ("5" in str(prime)):
            continue
        if is_circular_prime(primeTestList, prime):
            numberOfPrimes += 1

    print("Number of circular primes: %i" % numberOfPrimes)
