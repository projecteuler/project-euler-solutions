# -*- coding: utf-8 -*-
'''Take the number 192 and multiply it by each of 1, 2, and 3:

192 × 1 = 192
192 × 2 = 384
192 × 3 = 576
By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?'''
# coding: utf-8
from time import clock
from Primes import sieve_prime, prime_test
SUP       = 10 ** 6
max_c     = 6
timeStart = clock()
pList     = sieve_prime(SUP)
for s in xrange(len(pList)):
    if pList[s] * max_c > SUP:
        break
    for c in xrange(max_c, len(pList) - s - 1):
        num = sum(pList[s:s + c])
        if num > SUP:
            break
        if prime_test(num) and c > max_c:
            max_c, solution = c, num
print 'Run time: %.5fs.' % (clock() - timeStart)
print solution
