# -*- coding: utf-8 -*-
'''A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.

For example,

44 → 32 → 13 → 10 → 1 → 1
85 → 89 → 145 → 42 → 20 → 4 → 16 → 37 → 58 → 89

Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.

How many starting numbers below ten million will arrive at 89?'''
# NOTE:
#   dig_square_sum(123) == dig_square_sum(1) + dig_square_sum(2)\
#       + dig_square_sum
#
from time import clock
from itertools import combinations_with_replacement as cwr
squareList = [(x ** 2) for (x) in range(10)] # [0, 1, 4 .. 81]
TFList     = [0] * 568 # dig_square_sum(10 ** 7 - 1) = 7 * 9 ** 2 = 567
def dig_square_sum(intNum):
    '''
    TARGET: get the dig_square_sum of a certain intNum
    METHOD: Module 10 and square it, loop till nothing left.
    '''
    ansNum = 0
    # Loop Invairant: current number that is wating for module.
    # Start: intNum, itself. TRUE
    # Pross: drop the last digit of current number. TRUE
    # Stop : 0. TRUE
    while intNum:
        ansNum += squareList[intNum % 10]
        intNum /= 10
    return ansNum
timeStart  = clock()
def main():
    # generate the TFList (True and False List)
    # Loop Invirant: x: How many numbers that we have already tested.
    # Start: x = 0. TRUE
    # Pross: apparent, TRUE
    # Stop : x = 567, TRUE
    for x in xrange(0, 568):
        if not x: continue
        currentNum = x
        while 1:
            if x == 1:
                break
            elif x == 89 or TFList[x]:
                TFList[currentNum] = 1
                break
            else:
                x = dig_square_sum(x)
    workingDict = {0:1}
    # Loop Invariant: i: the i-digits numbers that we have calc
    #                 workingDict:
    #                   key: the number of dig_square_sum(intNum)
    #                   value: how many intNums that will dig_square_sum to key
    # Start: i = 0, TRUE
    # Pross: apparent, TRUE
    # Stop : i = 7, TRUE
    for i in xrange(7): # number of digits
        nextTurn = dict()
        for total, count in workingDict.iteritems():
            for square in squareList:
                nextTotal = total + square
                try:
                    nextTurn[nextTotal] += count
                except KeyError:
                    nextTurn[nextTotal] = count
        workingDict = nextTurn
    fin = 0
    for key, value in workingDict.iteritems():
        fin += TFList[key] and value or 0
    print fin
main()
print 'Run time: %.3fms.' % ((clock() - timeStart) * 1000)
