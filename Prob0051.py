# -*- coding: utf-8 -*-
'''
By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.

Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
'''
'''
Tips:

If we have an eight prime value family, the replacing part of the least number must be one of `0', `1' or `2'. Otherwise, suppose the least number has `3' as the replacing part, only 7 numbers would be in the family at most.

3 is a prime number, and whether a postive integer could be divided by 3 is of ease to judge. Considering that if a integer greater than 3, and could be divided by 3, it cannot be a prime.

Suppose we have a postive integer, with a r-digit replacing part. Denote k as the residue of the sum of the integer's digits, excluding the r-digit replacing part. (Hence k in {0, 1, 2}) We can compute the number of the numbers who could not be divided by 3, in the set of numbers, which is consisted by replacing the r-digit of the begining number from `0' to `9'. After that, an agreeable fact surfaced:
If and only if r is in the set {3, 6, 9}, we'll have a family that contains more than 8 numbers (actually, we will get 10 numbers in it).

Hence:
* is a prime more than 10
* excluding the last digit, there should be three or more `0', `1' or `2'
'''

from itertools import combinations as combi
from Primes import get_prime_test_list as gptl

ptList = gptl()

# generate a list like [1, 0, 0, 1, 1, 0]
# representing *ab**c
def generate_Mask(bit):
    for mask_bit in xrange(3, bit, 3):
        # mask_bit should be a multiple of 3
        for tupIdx in combi(range(bit - 1), mask_bit):
            outList = [0] * bit
            for idx in tupIdx:
                outList[idx] = 1
            yield outList

# test if the numbers in the postion specified by Mask are the same
def mask_is_same_number(strNum, mask):
    num = [strNum[x] for x in xrange(len(mask)) if mask[x] == 1]
    return len(set(num)) == 1

# Counting the number of primes after replacing
def count_primes(strNum, mask):
    count = 0
    for dig in xrange(0 + (mask[0]==1), 10):
        strNum = [str(dig) if mask[x] == 1 else strNum[x]
                   for x in range(len(mask))]
        count += ptList[int(''.join(strNum))]
    return count

# test if a number is a member of an eight prime value family
def run_test(num):
    strNum = str(num)
    for mask in generate_Mask(len(strNum)):
        if mask_is_same_number(strNum, mask):
            if count_primes(strNum, mask) >= 8:
                return True
    return False

# main process
workingNum = 1001
while 1:
    if not ptList[workingNum]:
        pass
    # wether has more than three `0`, `1' or `2'
    elif not (str(workingNum).count('0')>=3 or \
              str(workingNum).count('1')>=3 or \
              str(workingNum).count('2')>=3):
        pass
    elif not run_test(workingNum):
        pass
    else:
        # get the least number
        print workingNum
        break
    workingNum += 1
