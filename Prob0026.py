# -*- coding: utf-8 -*-
'''A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2 =   0.5
1/3 =   0.(3)
1/4 =   0.25
1/5 =   0.2
1/6 =   0.1(6)
1/7 =   0.(142857)
1/8 =   0.125
1/9 =   0.(1)
1/10    =   0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.'''
def length_of_loop_knot(intNum):
    workingList = list()
    dividend    = 10
    while 1:
        dividend = dividend % intNum * 10
        if dividend in workingList:
            return len(workingList[workingList.index(dividend):])
        else:
            workingList.append(dividend)
lenMax   = 0
storeNum = 0
for x in xrange(999, 2, -2):
    if x > lenMax and x % 5:
        temp = length_of_loop_knot(x)
        if temp > lenMax:
            lenMax   = temp
            storeNum = x

if __name__ == '__main__':
    print storeNum
