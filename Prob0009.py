# -*- coding: utf-8 -*-
'''
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
'''
def main():
    for x in xrange(1, 1000):
        for y in xrange(x, 1000 - x):
            for z in xrange(y, 1000 - y + 1):
                if not x + y + z == 1000:
                    pass
                elif not x ** 2 + y ** 2 == z ** 2:
                    pass
                else:
                    print x * y * z
                    return None
if __name__ == '__main__':
    main()
