# -*- coding: utf-8 -*-
'''The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?'''
from time import clock
from Primes import get_prime_test_list
SUP       = 10 ** 4
INF       = 10 ** 3 + 1
is_permu  = lambda intA, intB: sorted(str(intA)) == sorted(str(intB))
timeStart = clock()
ptList    = get_prime_test_list(SUP)
for x in xrange(INF, SUP, 2):
    if ptList[x]:
        for y in xrange(x + 2, (SUP + x) / 2, 2):
            if ptList[y] and is_permu(x, y) and ptList[2 * y - x] and is_permu(x, 2 * y - x):
                print str(x) + str(y) + str(2 * y - x)
print 'Run time: %.5fs.' % (clock() - timeStart)
