# -*- coding: utf-8 -*-
'''
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
'''

gcd = lambda a, b: (b == 0) and (a) or (gcd(b, a % b))

nume, deno = 1, 1
for p in xrange(1, 10):
    for q in xrange(p + 1, 10):
        for n in xrange(1, 10):
            if (p * 10 + n) * q == (n * 10 + q) * p:
                nume *= p
                deno *= q
            if (n * 10 + p) * q == (q * 10 + n) * p:
                nume *= p
                deno *= q

print deno / gcd(nume, deno)
