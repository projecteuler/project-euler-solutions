# -*- coding: utf-8 -*-
'''
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
'''

from Primes import get_prime_test_list as pt
SUP = 2 * 10 ** 6
ptList = pt(SUP)
primeSum = 0
for x in xrange(SUP):
    if ptList[x]:
        primeSum += x

if __name__ == '__main__':
    print primeSum
